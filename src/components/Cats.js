import { useState} from 'react'
import { Card, CardTitle, CardText, Button } from 'reactstrap';

function Cats({ item:{name, description, origin, wikipedia_url,temperament }}) {
    const title = {
        alignSelf: 'center',
      }
    
    const [readMore,setReadMore]=useState(false)
    const linkName=readMore?<Button outline color="secondary">Collapse</Button>:<Button outline color="success">Detail</Button>
    return (
        <div className="col-sm-4 my-2" >
            <Card body>
                <CardTitle tag="h3" style={title}>{name}</CardTitle>
                <CardText><b>Origin :</b>  {origin}</CardText>
                <CardText> <b>Temperament :</b> {temperament}</CardText>
                <CardText> <b>Wikipedia :</b> <a href={wikipedia_url} target='_blank' rel="noreferrer">{wikipedia_url}</a></CardText>
                <CardText>
                { readMore && description}
                    <a className="read-more-link" href="#down" onClick={()=>{setReadMore(!readMore)}}><h2>{linkName}</h2></a>
                </CardText>
            </Card>
        </div>
    )
}

export default Cats
