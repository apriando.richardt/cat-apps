
function Loader() {
    return (
        <div className="container">
            <div className="row">
                <div className="col d-flex justify-content-center my-5">
                <div className="spinner-grow text-danger" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
                <div className="spinner-grow text-warning" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
                <div className="spinner-grow text-info" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
                </div>
            </div>
        </div>
    )
}

export default Loader
