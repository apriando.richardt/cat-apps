import { useState, useEffect} from 'react'
import './App.css'
import InfiniteScroll from 'react-infinite-scroll-component'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  FormGroup,
  Input
} from 'reactstrap'
import logo from "../src/assets/img/logo.svg"
import Cats from './components/Cats'
import Loader from './components/Loader'
import EndMsg from './components/EndMsg'

function App() {

  const [isOpen, setIsOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState('')
  const toggle = () => setIsOpen(!isOpen);
  const vert_align = {
    display: 'flex',
    flexDirection: 'row'
  }
  const ml ={
    marginLeft:10
  }

  const ml2 ={
    marginLeft:25
  }


  const  [items, setItems] = useState([])
  const [noMore, setnoMore] = useState(true)
  const [page, setPage] = useState(2)

  const margin = {
    marginTop:90
  }

  useEffect(() => {
    const getCats = async () => {
      const res = await fetch(`https://api.thecatapi.com/v1/breeds?page=1&limit=10`)
      const data = await res.json()
      setItems(data)
    }
    getCats()
  }, [])

  // console.log('data items:',items);

  const fetchCats = async () => {
    
    const res = await fetch(`https://api.thecatapi.com/v1/breeds?page=${page}&limit=10`)
    const data = await res.json()
    return data
  }

  const fetchData = async () => {
    const catsFromServer = await fetchCats()

    setItems([...items, ...catsFromServer])

    if(catsFromServer.length === 0 || catsFromServer.length < 10){
      setnoMore(false)
    }
    setPage(page+1)
  }

  return (
    <InfiniteScroll
        dataLength={items.length}
        next={fetchData}
        hasMore={noMore}
        loader={<Loader/>}
        endMessage={<EndMsg/>}
        style={margin}
      >
        <Navbar color="dark" dark expand="md" style={vert_align} fixed='top'>
        <NavbarBrand href="/" style={ml2}> <img src={logo} alt="logo" />  Cats Apps </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <FormGroup className="ml-2" style={ml}>
              <Input type="text" name="search" id="search" placeholder="Search Breeds . . ." onChange={event => {setSearchTerm(event.target.value)}} />
            </FormGroup>
          </Collapse>
      </Navbar>
        <div className="container">
          <div className="row m-2">
              {items.filter((val) => {
                if(searchTerm === ""){
                  return val
                }else if(
                  val.name.toLowerCase().includes(searchTerm.toLocaleLowerCase())
                  ){
                  return val
                }
                return false
              }).map((item) =>{
                return <Cats key={item.id} item={item}/>
              })}
          </div>
        </div>
        
    </InfiniteScroll>
  );
}

export default App;
